'use strict';

module.exports = function connectToDatabase(uri, options) {
    if (cachedDb && cachedDb.serverConfig.isConnected()) {
        console.log('=> using cached database instance');
        return Promise.resolve(cachedDb);
    }

    return mongoose.connect(uri, options).then(db => {
        cachedDb = db;return cachedDb;
    });
};