'use strict';

module.exports = function (dbConfig) {
  let connection = {};
  connection.url = `mongodb://${dbConfig.user}:${dbConfig.password}@${dbConfig.server}:${dbConfig.port}/${dbConfig.db}`;
  connection.options = dbConfig.options;
  if (dbConfig.hosts && Array.isArray(dbConfig.hosts)) {
    let hostConnection = dbConfig.hosts.map(host => `${host.name}:${host.port || 27017}`).join(',');
    connection.url = `mongodb://${dbConfig.user}:${dbConfig.password}@${hostConnection}/${dbConfig.db}?replicaSet=${dbConfig.replicaSet}`;
    /*connection.options = Object.assign({}, dbConfig.options, {
      db: {native_parser: true},
      replset: {
        auto_reconnect:false,
        poolSize: 10,
        socketOptions: {
          keepAlive: 1000,
          connectTimeoutMS: 30000
        }
      },
      server: {
        poolSize: 5,
        socketOptions: {
          keepAlive: 1000,
          connectTimeoutMS: 30000
        }
      }
    });*/
  }
  return connection;
};