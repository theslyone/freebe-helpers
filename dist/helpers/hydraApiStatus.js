'use strict';

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (resp) {
    // console.log('|---------------------------|')
    // console.log(resp);
    // console.log('|---------------------------|')
    if (resp.statusCode >= _httpStatus2.default.OK && resp.statusCode < 300) {
        return resp.result || resp.statusMessage;
    }
    //console.error(resp.errors);
    throw new Error(resp.statusDescription);
};