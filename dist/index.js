'use strict';

var _hydraApiStatus = require('./helpers/hydraApiStatus');

var _hydraApiStatus2 = _interopRequireDefault(_hydraApiStatus);

var _APIError = require('./helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

var _Cacher = require('./helpers/Cacher');

var _Cacher2 = _interopRequireDefault(_Cacher);

var _getDatabaseConnection = require('./helpers/getDatabaseConnection');

var _getDatabaseConnection2 = _interopRequireDefault(_getDatabaseConnection);

var _makeAPIRequest = require('./helpers/makeAPIRequest');

var _makeAPIRequest2 = _interopRequireDefault(_makeAPIRequest);

var _authentication = require('./middlewares/authentication');

var _enforceHTTPS = require('./middlewares/enforceHTTPS');

var _enforceHTTPS2 = _interopRequireDefault(_enforceHTTPS);

var _getDatabase = require('./middlewares/getDatabase');

var _getDatabase2 = _interopRequireDefault(_getDatabase);

var _expressExtension = require('./helpers/expressExtension');

var _expressExtension2 = _interopRequireDefault(_expressExtension);

var _umfmessage = require('./lib/umfmessage');

var _umfmessage2 = _interopRequireDefault(_umfmessage);

var _utils = require('./lib/utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = {
    UMFMessage: _umfmessage2.default,
    Utils: _utils2.default,
    makeAPIRequest: _makeAPIRequest2.default,
    expressExtension: _expressExtension2.default,
    getDatabaseConnection: _getDatabaseConnection2.default,
    hydraApiStatus: _hydraApiStatus2.default,
    APIError: _APIError2.default,
    Cacher: _Cacher2.default,
    validateJwtToken: _authentication.validateJwtToken,
    authenticationRequired: _authentication.authenticationRequired,
    ensureUserEnabled: _authentication.ensureUserEnabled,
    ensureIsAdmin: _authentication.ensureIsAdmin,
    getDatabase: _getDatabase2.default,
    enforceHTTPS: _enforceHTTPS2.default
};