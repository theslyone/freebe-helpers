'use strict';

let verifyAuthorization = (() => {
  var _ref = _asyncToGenerator(function* (req, res, next) {
    var bearerHeader = req.headers.authorization;
    if (bearerHeader) {
      var parts = bearerHeader.split(' ');
      if (parts.length === 2) {
        var scheme = parts[0];
        var token = parts[1];

        if (/^Bearer$/i.test(scheme)) {
          if (/^fb_skey/i.test(token)) {
            let message = _umfmessage2.default.createMessage({
              authorization: `${req.headers.authorization}`,
              to: `user-service:[POST]/user/apiToken`,
              from: 'user-service',
              body: {
                apiToken: token
              }
            });

            try {
              let resp = (0, _hydraApiStatus2.default)((yield (0, _makeAPIRequest2.default)(process.env.USER_SERVICE_DOMAIN, message)));
              if (resp.token) {
                //req.headers.authorization = `Bearer ${resp.token}`;
                req.user = resp.user;
                return next();
              }
            } catch (error) {
              return next(error);
            }
          } else {
            return next();
          }
        }
      }
    }
    return next(new _APIError2.default('Bearer token not found', _httpStatus2.default.UNAUTHORIZED));
  });

  return function verifyAuthorization(_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
})();

var _fwspJwtAuth = require('fwsp-jwt-auth');

var _fwspJwtAuth2 = _interopRequireDefault(_fwspJwtAuth);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

var _makeAPIRequest = require('../helpers/makeAPIRequest');

var _makeAPIRequest2 = _interopRequireDefault(_makeAPIRequest);

var _hydraApiStatus = require('../helpers/hydraApiStatus');

var _hydraApiStatus2 = _interopRequireDefault(_hydraApiStatus);

var _umfmessage = require('../lib/umfmessage');

var _umfmessage2 = _interopRequireDefault(_umfmessage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

let validateJwtToken = (req, res, next) => {
  let authHeader = req.headers.authorization;
  if (!authHeader) {
    res.sendUnauthorizedRequest('Invalid token');
  } else {
    let token = authHeader.split(' ')[1];
    if (token) {
      return _fwspJwtAuth2.default.verifyToken(token).then(decoded => {
        req.authToken = decoded;
        next();
      }).catch(err => {
        res.sendUnauthorizedRequest(err.message);
      });
    } else {
      res.sendUnauthorizedRequest('Invalid token');
    }
  }
};

function ensureUserEnabled(req, res, next) {
  let message = _umfmessage2.default.createMessage({
    authorization: `${req.headers.authorization}`,
    to: `user-service:[GET]/user/${req.user._id}/isEnabled`,
    from: 'user-service',
    body: {}
  });

  (0, _makeAPIRequest2.default)(process.env.USER_SERVICE_DOMAIN, message).then(_hydraApiStatus2.default).then(isEnabled => {
    if (isEnabled === true) {
      next();
    } else {
      return res.sendUnauthorizedRequest('Your account has been deactivated!');
    }
  }).catch(error => {
    next(error);
  });
}

function ensureIsAdmin(req, res, next) {
  if (req.user && typeof Array.isArray(req.user.roles) && req.user.roles.includes('admin')) return next();
  next(new _APIError2.default('Admin role required, authorization denied', _httpStatus2.default.UNAUTHORIZED));
}

function authenticationRequired() {
  return [verifyAuthorization, (req, res, next) => {
    //console.log(JSON.stringify(req.user, null, 4));                        
    if (req.user) return next();
    validateJwtToken(req, res, next);
  }, (req, res, next) => {
    req.user = req.user || req.authToken;
    next();
  }];
}

module.exports = {
  validateJwtToken,
  authenticationRequired,
  ensureUserEnabled,
  ensureIsAdmin
};