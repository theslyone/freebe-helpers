import config from 'fwsp-config';

module.exports = function (connection, service) {
    return function (req, res, next) {
        let hostname = req.hostname.split(':')[0].replace(/\./g, '_')
        if(hostname == '127_0_0_1' || hostname == 'localhost')
            hostname = 'localhost'
        // console.log('-----------DATABASE-------------')
        // console.log(hostname)
        config.db = connection.getDb(`${hostname}_${service}_${process.env.NODE_ENV || 'development'}`)
        next()
    }
}