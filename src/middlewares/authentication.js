import jwtAuth from 'fwsp-jwt-auth';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import makeAPIRequest from '../helpers/makeAPIRequest';
import hydraApiStatus from '../helpers/hydraApiStatus';
import UMFMessage from '../lib/umfmessage';

let validateJwtToken = (req, res, next) => {
  let authHeader = req.headers.authorization;
  if (!authHeader) {
    res.sendUnauthorizedRequest('Invalid token')
  } else {
    let token = authHeader.split(' ')[1];
    if (token) {
      return jwtAuth.verifyToken(token)
        .then((decoded) => {
          req.authToken = decoded;
          next();
        })
        .catch((err) => {
          res.sendUnauthorizedRequest(err.message)
        });
    } else {
      res.sendUnauthorizedRequest('Invalid token')
    }
  }
}

async function verifyAuthorization(req, res, next) {
  var bearerHeader = req.headers.authorization;
  if (bearerHeader) {
    var parts = bearerHeader.split(' ');
    if (parts.length === 2) {
      var scheme = parts[0];
      var token = parts[1];

      if (/^Bearer$/i.test(scheme)) {
        if (/^fb_skey/i.test(token)) {
          let message = UMFMessage.createMessage({
            authorization: `${req.headers.authorization}`,
            to: `user-service:[POST]/user/apiToken`,
            from: 'user-service',
            body: {
              apiToken: token
            }
          });

          try {
            let resp = hydraApiStatus(await makeAPIRequest(process.env.USER_SERVICE_DOMAIN, message));
            if (resp.token) {
              //req.headers.authorization = `Bearer ${resp.token}`;
              req.user = resp.user;
              return next();
            }
          }
          catch (error) {
            return next(error);
          }
        } else {
          return next();
        }
      }
    }
  }
  return next(new APIError('Bearer token not found', httpStatus.UNAUTHORIZED));
}

function ensureUserEnabled(req, res, next) {
  let message = UMFMessage.createMessage({
    authorization: `${req.headers.authorization}`,
    to: `user-service:[GET]/user/${req.user._id}/isEnabled`,
    from: 'user-service',
    body: {}
  })

  makeAPIRequest(process.env.USER_SERVICE_DOMAIN, message)
    .then(hydraApiStatus)
    .then((isEnabled) => {
      if (isEnabled === true) {
        next();
      } else {
        return res.sendUnauthorizedRequest('Your account has been deactivated!');
      }
    })
    .catch((error) => {
      next(error);
    });
}

function ensureIsAdmin(req, res, next) {
  if (req.user && typeof Array.isArray(req.user.roles) && req.user.roles.includes('admin')) return next();
  next(new APIError('Admin role required, authorization denied', httpStatus.UNAUTHORIZED));
}

function authenticationRequired() {
  return [
    verifyAuthorization,
    (req, res, next) => {
      //console.log(JSON.stringify(req.user, null, 4));                        
      if (req.user) return next();
      validateJwtToken(req, res, next);
    },
    (req, res, next) => {
      req.user = req.user || req.authToken;
      next();
    }
  ]
}


module.exports = {
  validateJwtToken,
  authenticationRequired,
  ensureUserEnabled,
  ensureIsAdmin
};