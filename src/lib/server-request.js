const fetch = require("node-fetch");

const REQUEST_TIMEOUT = 30000; // 30-seconds

/**
 * @name ServerRequest
 * @summary Class for handling server requests
 */
class ServerRequest {
  /**
  * @name constructor
  * @summary Class constructor
  * @return {undefined}
  */
  constructor() {
  }

  /**
  * @name send
  * @summary sends an HTTP Request
  * @param {object} options - request options
  * @return {object} promise
  */
  send(url, options) {
    if (options.method === 'POST' || options.method === 'PUT') {
      options.headers = options.headers || {};
      options.headers['content-length'] = Buffer.byteLength(options.body, 'utf8');
    } else {
      delete options.body;
    }

    return fetch(url, options)
      .then(res => res.json())
  }
}

module.exports = ServerRequest;
