import httpStatus from 'http-status';

module.exports = function (resp) {
    // console.log('|---------------------------|')
    // console.log(resp);
    // console.log('|---------------------------|')
    if(resp.statusCode >= httpStatus.OK && resp.statusCode < 300) {
        return resp.result || resp.statusMessage;
    }
    //console.error(resp.errors);
    throw new Error(resp.statusDescription);
}