const Utils = require('../lib/utils');
const UMFMessage = require('../lib/umfmessage');

const ServerResponse = require('../lib/server-response');
const ServerRequest = require('../lib/server-request');

let serverResponse = new ServerResponse();
let serverRequest = new ServerRequest();

const UMF_INVALID_MESSAGE = 'UMF message requires "to", "from" and "body" fields';

module.exports = (domain = 'https://api.freebe.com.ng', message = {}, sendOpts = {}) => {
    return new Promise((resolve, reject) => {
        let umfmsg = UMFMessage.createMessage(message);
        if (!umfmsg.validate()) {
            reject(UMF_INVALID_MESSAGE);
            return;
        }

        let parsedRoute = UMFMessage.parseRoute(umfmsg.to);
        if (parsedRoute.error) {
            reject(parsedRoute.error);
            return;
        }

        if (!parsedRoute.httpMethod) {
            reject('HTTP method not specified in `to` field');
            return;
        }

        if (parsedRoute.apiRoute === '') {
            reject('message `to` field does not specify a valid route');
            return;
        }
        let url =  `${domain}${parsedRoute.apiRoute}`  
        let options = {
            method: parsedRoute.httpMethod.toUpperCase()
        };
        let preHeaders = {};
        if (options.method === 'POST' || options.method === 'PUT') {
            preHeaders['Accept'] = 'application/json'      
            preHeaders['content-type'] = 'application/json';
        }
        options.headers = Object.assign(preHeaders, umfmsg.headers);
        if (umfmsg.authorization) {
            options.headers.Authorization = umfmsg.authorization;
        }
        if (umfmsg.timeout) {
            options.timeout = umfmsg.timeout;
        }
        options.body = Utils.safeJSONStringify(umfmsg.body);
        options = Object.assign({}, options, sendOpts)
        serverRequest.send(url, options)
            .then((res) => {
                resolve(serverResponse.createResponseObject(res.statusCode, res));
            })
            .catch((err) => {
                reject(err)
            });
    });
}