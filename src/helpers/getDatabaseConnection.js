'use strict';

module.exports = function (dbConfig) {
  let connection = {};
  connection.url = `mongodb://${dbConfig.server}:${dbConfig.port}/${dbConfig.db}`;
  connection.options = Object.assign({}, {
    user: dbConfig.user,
    pass: dbConfig.password
  }, dbConfig.options);
  if (dbConfig.hosts && Array.isArray(dbConfig.hosts)) {
    let hostConnection = dbConfig.hosts.map(host => `${host.name}:${host.port || 27017}`).join(',');
    connection.url = `mongodb://${hostConnection}/${dbConfig.db}?replicaSet=${dbConfig.replicaSet}`;
  }
  return connection;
};