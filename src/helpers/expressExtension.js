const ServerResponse = require('fwsp-server-response');
let serverResponse = new ServerResponse();

module.exports = function (express) {
    express.response.sendError = function (err) {
        serverResponse.sendServerError(this, { statusDescription: err });
    };
    express.response.sendBadRequest = function (message) {
        serverResponse.sendInvalidRequest(this, { statusDescription: message });
    };
    express.response.sendUnauthorizedRequest = function (message) {
        serverResponse.sendInvalidUserCredentials(this, { statusDescription: message });
    };
    express.response.sendOk = function (result) {
        serverResponse.sendOk(this, { result });
    };
    express.response.sendCreated = function (result) {
        serverResponse.sendCreated(this, { result });
    };
    express.response.sendNotFound = function (result) {
        serverResponse.sendNotFound(this, { result });
    };
}