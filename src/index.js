import hydraApiStatus from './helpers/hydraApiStatus';
import APIError from './helpers/APIError';
import Cacher from './helpers/Cacher';
import getDatabaseConnection from './helpers/getDatabaseConnection';
import makeAPIRequest from './helpers/makeAPIRequest';
import {
    validateJwtToken,
    authenticationRequired,
    ensureUserEnabled,
    ensureIsAdmin
} from './middlewares/authentication';
import enforceHTTPS from './middlewares/enforceHTTPS';
import getDatabase from './middlewares/getDatabase';
import expressExtension from './helpers/expressExtension'
import UMFMessage from './lib/umfmessage'
import Utils from './lib/utils'

module.exports = {
    UMFMessage,
    Utils,
    makeAPIRequest,
    expressExtension,
    getDatabaseConnection,
    hydraApiStatus,
    APIError,
    Cacher,
    validateJwtToken,
    authenticationRequired,
    ensureUserEnabled,
    ensureIsAdmin,
    getDatabase,
    enforceHTTPS
};